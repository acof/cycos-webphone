package com.example.cycoswebphone

import kotlinx.serialization.Serializable

@Serializable
class ClientStateUpdate(
    val registrationState: String,
    var connectState: String,
    val audioMuted: Boolean,
    val videoMuted: Boolean,
    val failReason: String = ""
) {
    override fun toString(): String {
        return "WebPhone received state: (registrationState='$registrationState', connectState='$connectState', failReason='$failReason', audioMuted=$audioMuted, videoMuted=$videoMuted)"
    }
}
