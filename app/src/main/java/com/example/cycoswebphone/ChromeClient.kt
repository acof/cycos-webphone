package com.fastviewer.msav.util

import android.app.Activity
import android.os.Build
import android.webkit.*
import androidx.annotation.RequiresApi


internal class ChromeClient(val activity: Activity) : WebChromeClient() {

    override fun onJsAlert(view: WebView, url: String, message: String, result: JsResult): Boolean {
        return true
    }

    override fun onJsConfirm(
        view: WebView,
        url: String,
        message: String,
        result: JsResult
    ): Boolean {
        return true
    }

    override fun onJsPrompt(
        view: WebView, url: String, message: String, defaultValue: String,
        result: JsPromptResult
    ): Boolean {
        return true
    }

    override fun onConsoleMessage(consoleMessage: ConsoleMessage?): Boolean {
        return super.onConsoleMessage(consoleMessage)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onPermissionRequest(request: PermissionRequest) {
        request.grant(request.resources)
    }

}