package com.example.cycoswebphone

import JavaScriptConnector
import JavascriptListener
import WebViewClient
import WebViewHandler
import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.net.http.SslError
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.WebResourceError
import android.webkit.WebView
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.example.cycoswebphone.databinding.ActivityMainBinding
import com.fastviewer.msav.util.ChromeClient
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlin.random.Random

class MainActivity : AppCompatActivity(), WebViewHandler, JavascriptListener {

    private lateinit var binding: ActivityMainBinding

    private lateinit var jsConnector: JavaScriptConnector

    private var clientUpdate: ClientStateUpdate? = null
    private var connectionParams: ConnectionParams? = null

    private var tryingToConnect = false

    private val json = Json {
        ignoreUnknownKeys = true
    }

    companion object {
        const val RC_HD_AUDIO_VIDEO = 42

        const val TAG = "CycosWebPhone"

        // Connection States
        private const val STATE_PREPARING = "preparing"
        private const val STATE_CONNECTING = "connecting"
        private const val STATE_CONNECTED = "connected"

        private const val STATE_DISCONNECTED = "disconnected"
        private const val STATE_FAILED = "failed"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
    }

    private fun hasPermissions(): Boolean {
        val audioPermission =
            ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
        val audioPermissionGranted = audioPermission == PackageManager.PERMISSION_GRANTED

        val videoPermission =
            ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val videoPermissionGranted = videoPermission == PackageManager.PERMISSION_GRANTED

        if (audioPermissionGranted && videoPermissionGranted) {
            return true
        }

        return false
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA),
            RC_HD_AUDIO_VIDEO
        )
    }

    fun onConnectClick(view: View) {
        connectToRoom()
    }

    private fun connectToRoom() {
        // no error handling here in the demo, they need to be accepted...
        val hasPermissions = hasPermissions()
        if (!hasPermissions) {
            requestPermissions()
            return
        }

        initWebView()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == RC_HD_AUDIO_VIDEO) {
            val audioPermissionGranted = grantResults[0] == PackageManager.PERMISSION_GRANTED
            val videoPermissionGranted = grantResults[1] == PackageManager.PERMISSION_GRANTED

            val permissionsGranted = audioPermissionGranted && videoPermissionGranted

            if (permissionsGranted) {
                initWebView()
            }
        }
    }

    private fun initWebView() {
        Log.d(TAG, "initWebView()")

        addWebViewSettings()
        initConnectionParams()

        binding.webview.webViewClient = WebViewClient(this)
        binding.webview.webChromeClient = ChromeClient(this)

        jsConnector = JavaScriptConnector(binding.webview, this)
        binding.webview.addJavascriptInterface(jsConnector, "Android")

        connectionParams?.let {
            binding.webview.loadUrl(it.url)
        }
    }

    private fun initConnectionParams() {
        val pin = binding.etRoomName.text.toString()
        val webPhoneURL = "https://ms1-av.fastviewer.com/webphone-fastviewer/?lang=EN"
        val localClientId = Random.nextInt(0, 5000)

        val address = "fvw-${pin}-${localClientId}"
        connectionParams =
            ConnectionParams(
                url = webPhoneURL,
                room = pin,
                address = address,
                clientName = "max"
            )
    }

    private fun addWebViewSettings() {
        val settings = binding.webview.settings

        // Enable javascript
        settings.javaScriptEnabled = true

        // Allow use of local storage
        settings.domStorageEnabled = true

        // Enable remote debugging via chrome://inspect
        if (BuildConfig.DEBUG) {
            WebView.setWebContentsDebuggingEnabled(true)
        }

        // allow zoom
        settings.setSupportZoom(true)
        settings.builtInZoomControls = true
        settings.displayZoomControls = false

        settings.mediaPlaybackRequiresUserGesture = false
    }

    override fun onPageStarted() {
        jsConnector.setup()
    }

    override fun onPageFinished() {
        Log.d(TAG, "onPageFinished")

        binding.etRoomName.hideKeyboard()
        binding.llConnect.visibility = View.GONE
        binding.llWebviewContainer.visibility = View.VISIBLE
        binding.tvRoomName.text = binding.etRoomName.text.toString()

        jsConnector.adjustSettings()

        if (clientUpdate?.connectState == STATE_DISCONNECTED) {
            connectMSAV()
        }

        ensureConnectCall()
    }

    // in case we missed the "DISCONNECTED" state because the javascript connector was
    // not yet set up - this triggers another onClientStateUpdated
    private fun ensureConnectCall() {
        Handler(Looper.getMainLooper()).postDelayed({
            if (clientUpdate == null || clientUpdate?.connectState.isNullOrBlank()) {
                jsConnector.getState()
            }
        }, 2000)
    }

    private fun connectMSAV() {
        if (tryingToConnect) {
            return
        }

        connectionParams?.let {
            tryingToConnect = true
            jsConnector.connect(params = it)
        }
    }

    override fun onReceivedError(error: WebResourceError?) {
    }

    override fun onSSLError(error: SslError?) {
    }

    override fun onClientStateUpdated(information: String) {
        clientUpdate = json.decodeFromString<ClientStateUpdate>(information)

        if (clientUpdate?.connectState == STATE_DISCONNECTED) {
            connectMSAV()
        }

        if (clientUpdate?.connectState == STATE_CONNECTED || clientUpdate?.connectState == STATE_FAILED) {
            tryingToConnect = false
        }

        binding.tvConnectionState.text = clientUpdate?.connectState
    }

    override fun onParticipantJoined(information: String) {
    }

    override fun onParticipantLeft(information: String) {
    }

    override fun onDestroy() {
        disconnect()
        super.onDestroy()
    }

    private fun disconnect() {
        jsConnector.disconnect()
    }

}


fun EditText.hideKeyboard(): Boolean {
    return (context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager)
        .hideSoftInputFromWindow(windowToken, 0)
}