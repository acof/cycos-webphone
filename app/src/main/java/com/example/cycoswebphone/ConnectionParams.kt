package com.example.cycoswebphone

data class ConnectionParams(
        val url: String,
        val audioMuted: Boolean = true,
        val videoMuted: Boolean = true,
        val room: String,
        val address: String,
        val clientName: String
)