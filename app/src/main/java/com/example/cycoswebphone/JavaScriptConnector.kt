import android.util.Log
import android.webkit.JavascriptInterface
import android.webkit.WebView
import com.example.cycoswebphone.ConnectionParams
import com.example.cycoswebphone.MainActivity.Companion.TAG

class JavaScriptConnector(
    private val webView: WebView,
    private var listener: JavascriptListener
) {

    companion object {
        // JavaScript interface
        private const val onClientStateUpdated = "onClientStateUpdated"
        private const val onParticipantJoined = "onParticipantJoined"
        private const val onParticipantLeft = "onParticipantLeft"
        // const val isParticipantButtonHidden = "isParticipantButtonHidden"

        val interfaceMethods = arrayOf(onClientStateUpdated, onParticipantJoined, onParticipantLeft)
    }

    fun hideConnectButton() {
        hideButton("Connect")
    }

    private fun hideButton(buttonTag: String) {
        val hideElementString =
            "window.is${buttonTag}ButtonHidden =() => { return true; }"

        webView.post {
            webView.loadUrl("javascript:$hideElementString")
        }
    }

    fun hideParticipantButton() {
        hideButton("Participant")
    }

    fun setAudioMuteState(muted: Boolean) {
        val muteString = "window.muteAudio($muted)"

        Log.d(TAG, "Sent: $muteString")

        webView.post {
            webView.loadUrl("javascript:$muteString")
        }
    }

    fun disableMuteAudio(disable: Boolean) {
        val disableString = "window.disableMuteAudio($disable)"

        Log.d(TAG, "Sent: $disableString")

        webView.post {
            webView.loadUrl("javascript:$disableString")
        }
    }

    /**
     * Get state triggers onClientStateUpdated
     */
    fun getState() {
        val stateString = "if (window.getState) { window.getState(); }"

        Log.d(TAG, "Sent: $stateString")

        webView.post {
            webView.loadUrl("javascript:$stateString")
        }
    }

    fun setVideoMuteState(muted: Boolean) {
        val muteString = "window.muteVideo($muted)"

        Log.d(TAG, "Sent: $muteString")

        webView.post {
            webView.loadUrl("javascript:$muteString")
        }
    }

    fun disableMuteVideo(disable: Boolean) {
        val disableString = "window.disableMuteVideo($disable)"

        Log.d(TAG, "Sent: $disableString")

        webView.post {
            webView.loadUrl("javascript:$disableString")
        }
    }

    /**
     * Register all Javascript functions / callbacks
     */
    fun setup() {
        var setupString = ""
        interfaceMethods.forEach { methodName ->
            setupString += "window.$methodName = (param) => { Android.$methodName(JSON.stringify(param)); };"
        }

        webView.post {
            webView.loadUrl("javascript:$setupString")
        }
    }

    @JavascriptInterface
    fun onClientStateUpdated(msg: String) {
        Log.d(TAG, "Received onClientStateUpdated(): $msg")
        listener.onClientStateUpdated(msg)
    }

    @JavascriptInterface
    fun onParticipantJoined(msg: String) {
        Log.d(TAG, "Received onParticipantJoined(): $msg")
        listener.onParticipantJoined(msg)
    }

    @JavascriptInterface
    fun onParticipantLeft(msg: String) {
        Log.d(TAG, "Received onParticipantLeft(): $msg")
        listener.onParticipantLeft(msg)
    }

    fun connect(params: ConnectionParams) {
        val connectString = "window.connectCall(" +
                "${params.audioMuted}, " +
                "${params.videoMuted}, " +
                "'${params.address}', " +
                "'${params.clientName}', " +
                "'${params.room}'" +
                ")"

        Log.d(TAG, "Sent: $connectString")

        webView.post {
            webView.loadUrl("javascript:$connectString")
        }
    }

    fun disconnect() {
        val disconnectString = "window.disconnectCall()"

        Log.d(TAG, "Sent: $disconnectString")

        webView.post {
            webView.loadUrl("javascript:$disconnectString")
        }
    }

    fun setParticipantRights(username: String, audioAllowed: Boolean, videoAllowed: Boolean) {
        val rightsString = "window.setParticipantRights($username, $audioAllowed, $videoAllowed)"

        Log.d(TAG, "Sent: $rightsString")

        webView.post {
            webView.loadUrl("javascript:$rightsString")
        }
    }

    fun adjustSettings() {
        val settings = "window.setOptions(" +
                "{settings:" +
                "{videoPreview:false, swapAudioInputOutput: true}," +
                "ui:{showSpeakerIcon:false}" +
                "}" +
                ")"

        Log.d(TAG, "Sent: $settings")

        webView.post {
            webView.loadUrl("javascript:$settings")
        }
    }

}

interface JavascriptListener {

    fun onClientStateUpdated(information: String)
    fun onParticipantJoined(information: String)
    fun onParticipantLeft(information: String)

}