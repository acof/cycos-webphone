
import android.net.http.SslError
import android.webkit.WebResourceError

interface WebViewHandler {
    fun onPageStarted()
    fun onPageFinished()

    fun onReceivedError(error: WebResourceError?)
    fun onSSLError(error: SslError?)
}